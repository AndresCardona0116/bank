<div class="form-group row">
    <label class="col-md-3 form-control-label" for="text-input">Nombre</label>
    <div class="col-md-9">
        <input type="text" id="Nombre" name="Nombre" class="form-control" placeholder="Nombre de egreso" required >
        <span class="help-block">(*) Ingrese el nombre del egreso</span>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 form-control-label" for="text-input">Valor del egreso</label>
    <div class="col-md-9">
        <input type="number" id="Valor" name="Valor" class="form-control" placeholder="Valor del egreso" required>
        <span class="help-block">(*) Ingrese el valor del egreso</span>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
    <button type="submit" class="btn btn-success">Guardar</button>
</div>