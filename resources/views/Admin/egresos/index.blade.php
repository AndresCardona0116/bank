@extends('Admin.layouts.dashboard')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Egresos</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Gestor de los egresos, Aqui se podra ingresar, editar y eliminar los egresos</li>
            </ol>
            <a href="/cartera" class="btn btn-success btn-block">Ver valor en cartera</a>
            <button type="button" class="btn btn-warning btn-block text-white" data-toggle="modal" data-target="#modalNuevo">Crear Egreso</button>
            <br>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Lista de los egresos
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Identificador</th>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Usuario</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Identificador</th>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Usuario</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($egresos as $egreso)
                                    <tr>
                                        <td>{{$egreso->id}}</td>
                                        <td>{{$egreso->Nombre}}</td>
                                        <td>{{$egreso->Valor}}</td>
                                        <td>{{$egreso->name}}</td>
                                        <td>
                                            <button type="submit" class="btn btn-success btn-sm" data-id_egreso="{{$egreso->id}}" data-nombre="{{$egreso->Nombre}}" data-valor="{{$egreso->Valor}}" data-toggle="modal" data-target="#modalEditarEgreso">
                                                <i class="far fa-edit"></i>
                                            </button>    
                                        </td>
                                        <td>
                                            <form action="/egresos/{{$egreso->id}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                                            </form>
                                            
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--Inicio del modal agregar-->
    <div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Egreso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('egresos.store')}}" method="post" class="form-horizontal">
                       {{csrf_field()}}
                       @include('Admin.egresos.form') 
                    </form> 
                </div>
                
            </div>
            
        </div>
        
    </div>

    <div class="modal fade" id="modalEditarEgreso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar egreso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('egresos.update', 'test')}}" method="post" class="form-horizontal">
                        {{--  --}}
                        {{method_field('patch')}}
                        {{csrf_field()}}
                        
                        <input type="hidden" id="id_Egreso" name="id_Egreso" value="">

                        @include('Admin.egresos.form')
                    </form> 
                </div>
                
            </div>
            
        </div>
        
    </div>

@endsection