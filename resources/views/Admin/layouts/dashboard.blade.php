<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="/css/admin/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        
        {{-- Editor de text area --}}
        <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

        
        
    </head>
    <body class="sb-nav-fixed">

        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.html">Start Bootstrap</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="login.html">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>

        
            <div id="layoutSidenav">
                {{-- SideBar --}}
                <div id="layoutSidenav_nav">
                    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div class="sb-sidenav-menu">
                            <div class="nav">
                                <div class="sb-sidenav-menu-heading">Core</div>
                                <a class="nav-link" href="index.html">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    Dashboard
                                </a>
                                <a class="nav-link" href="index.html">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    Ingresos
                                </a>
                                <a class="nav-link" href="index.html">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    Egresos
                                </a>
                                <a class="nav-link" href="index.html">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    ...
                                </a>
                                
                            </div>
                        </div>
                        <div class="sb-sidenav-footer">
                            <div class="small">Logged in as:</div>
                            Start Bootstrap
                        </div>
                    </nav>
                </div>
                <div id="layoutSidenav_content">

                    @yield('content')
                    
                    
                </div>

            </div>
            
            

            

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/admin/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="js/admin/demo/chart-area-demo.js"></script>
        <script src="js/admin/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="js/admin/demo/datatables-demo.js"></script>

        


        {{-- Variables que controlan la cartera --}}
        @if (isset($ingresosC) || isset($egresosC)) 
            <script type="text/javascript">
                var ingresosjs = {{$ingresosC}};
                var egresosjs = {{$egresosC}};
                var ctx = document.getElementById("BarCartera");
                var myLineChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Ingresos", "Egresos"],
                    datasets: [{
                    label: "VALOR $",
                    backgroundColor: "rgba(2,117,216,1)",
                    borderColor: "rgba(2,117,216,1)",
                    data: [ingresosjs, egresosjs],
                    }],
                },
                options: {
                    scales: {
                    xAxes: [{
                        gridLines: {
                        display: false
                        },
                        ticks: {
                        maxTicksLimit: 2
                        }
                    }],
                    yAxes: [{
                        ticks: {
                        min: 0,
                        max: ingresosjs,
                        maxTicksLimit: 5
                        },
                        gridLines: {
                        display: true
                        }
                    }],
                    },
                    legend: {
                    display: false
                    }
                }
                });
            </script>
        @endif

            
        <script type="text/javascript">
            $('#modalEditar').on('show.bs.modal', function(event){
                var button = $(event.relatedTarget);
                var Nombre_Modal_Editar = button.data('nombre');
                var Valor_Modal_Editar = button.data('valor');
                var id_Ingreso = button.data('id_ingreso');
                var modal = $(this);
                
                //Llevar informacion al formulario segun las variables definidas
                
                modal.find('.modal-body #id_Ingreso').val(id_Ingreso);
                modal.find('.modal-body #Nombre').val(Nombre_Modal_Editar);
                modal.find('.modal-body #Valor').val(Valor_Modal_Editar); 
            })

            $('#modalEditarEgreso').on('show.bs.modal', function(event){
                var button = $(event.relatedTarget);
                var Nombre_Modal_Editar = button.data('nombre');
                var Valor_Modal_Editar = button.data('valor');
                var id_Egreso = button.data('id_egreso');
                var modal = $(this);
                
                //Llevar informacion al formulario segun las variables definidas
                modal.find('.modal-body #id_Egreso').val(id_Egreso);
                modal.find('.modal-body #Nombre').val(Nombre_Modal_Editar);
                modal.find('.modal-body #Valor').val(Valor_Modal_Editar); 
            })
        </script>
        
    </body>
</html>
