@extends('admin.layouts.dashboard')

{{-- Contenido desde el layout de admin --}}
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Ingresos</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Gestor de ingreso, Aqui podras agregar editar y eliminar ingresos</li>
            </ol>
            <a href="/cartera" class="btn btn-success btn-block">Ver valor en cartera</a>
            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modalNuevo">Crear Ingreso</button>
            <br>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Lista de ingresos
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Identificador</th>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Meta asig</th>
                                    <th>Usuario</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Identificador</th>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Meta asig</th>
                                    <th>Usuario</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($ingresos as $ingreso)
                                    <tr>
                                        <td>{{$ingreso->id}}</td>
                                        <td>{{$ingreso->Nombre}}</td>
                                        <td>{{$ingreso->Valor}}</td>
                                        <td>{{$ingreso->EstadoMeta}}</td>
                                        <td>{{$ingreso->name}}</td>
                                        <td>
                                            <button type="submit" class="btn btn-success btn-sm" data-id_ingreso="{{$ingreso->id}}" data-nombre="{{$ingreso->Nombre}}" data-valor="{{$ingreso->Valor}}" data-toggle="modal" data-target="#modalEditar">
                                                <i class="far fa-edit"></i>
                                            </button>    
                                        </td>
                                        <td>
                                            @if ($ingreso->EstadoMeta == 0)
                                                <form action="/ingresos/{{$ingreso->id}}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                                                </form>
                                            @else
                                                ASOCIADO A META
                                            @endif
                                            
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <!--Inicio del modal agregar-->
    <div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Ingreso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('ingresos.store')}}" method="post" class="form-horizontal">
                       {{csrf_field()}}
                       @include('Admin.ingresos.form') 
                    </form> 
                </div>
                
            </div>
            
        </div>
        
    </div>



    <div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-primary modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Ingreso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('ingresos.update', 'test')}}" method="post" class="form-horizontal">
                        {{method_field('patch')}}
                        {{csrf_field()}}
                        
                        <input type="hidden" id="id_Ingreso" name="id_Ingreso" value="">

                        @include('Admin.ingresos.form')
                    </form> 
                </div>
                
            </div>
            
        </div>
        
    </div>


@endsection