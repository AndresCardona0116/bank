@extends('admin.layouts.dashboard')

@section('content')
    <main>
        <div class="container-fluid">

            <h1 class="mt-4">Cartera</h1>
            
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Indicador de manejo de la cartera, Tango ingresos como egresos</li>
            </ol>
            <a href="/ingresos" class="btn btn-warning btn-block text-white">Regresar a ingresos</a>
            <br>
            <a href="/egresos" class="btn btn-warning btn-block text-white">Regresar a egresos</a>
            <br>

            <div class="row">
                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="card bg-success text-white d-flex align-items-center col-lg-12">
                            <div class="card-body"><a href="/ingresos" class="text-white">INGRESOS...</a></div>
                            <div class="card-footer">
                            <p class="text-white">${{$ingresosC}}</p>                        
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col-lg-12">
                        <div class="card bg-danger text-white d-flex align-items-center col-lg-12">
                            <div class="card-body"><a href="#" class="text-white">EGRESOS...</a></div>
                            <div class="card-footer">
                            <p class="text-white">${{$egresosC}}</p>                        
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-chart-bar mr-1"></i>
                                Ingresos VS Egresos 
                            </div>
                            <div class="card-body"><canvas id="BarCartera" width="100%" height="40"></canvas></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card bg-primary text-white d-flex align-items-center col-lg-12">
                        <div class="card-body"><a href="#" class="text-white">DINERO EN CARTERA...</a></div>
                        <div class="card-footer">
                        <p class="text-white">${{$ingresosC - $egresosC}}</p>                        
                        </div>
                    </div>
                </div>
            </div>
    </main>

    
@endsection