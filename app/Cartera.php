<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartera extends Model
{
    //Modelo para el apartado de cartera 
    protected $table = 'movimientos';

    protected $fillable = ['Nombre', 'Valor', 'Estado', 'EstadoMeta', 'TipoMovimiento', 'Usuario'];
}
