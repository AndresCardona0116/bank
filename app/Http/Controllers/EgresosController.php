<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Egresos;
use Illuminate\Support\Facades\Redirect;
use DB;

class EgresosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Se muestra toda la informacion y se redireje a la pestaña
        $egresos = Egresos::join('users', 'movimientos.Usuario','=','users.id')
                    ->select('movimientos.id', 'movimientos.Nombre', 'movimientos.Valor', 'movimientos.Estado', 'movimientos.TipoMovimiento', 'users.name')
                    ->where([
                        ['TipoMovimiento', '=', '2'],
                        ['Estado', '=', '1'],
                    ])->orderBy('Id', 'desc')
                    ->Get();

        return view('Admin.egresos.index', ['egresos' => $egresos]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Se inserta nuevo egreso a la BD
        $Egresos = New Egresos();
        $Egresos->Nombre = $request->Nombre;
        $Egresos->Valor = $request->Valor;
        $Egresos->Estado = '1';
        $Egresos->EstadoMeta = '0';
        $Egresos->TipoMovimiento = '2';
        $Egresos->Usuario = '1';
        $Egresos->save();
        return Redirect::to('egresos');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Editar los datos de los egresos segun id 
        $egresos = Egresos::findOrFail($request->id_Egreso);
        $egresos->Nombre = $request->Nombre;
        $egresos->Valor = $request->Valor;
        $egresos->Estado = '1';
        $egresos->EstadoMeta = '0';
        $egresos->TipoMovimiento = '2';
        $egresos->Usuario = '1';
        $egresos->save();
        return Redirect::to('egresos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Egresos $egreso)
    {
        //Se modifican el ingreso para que no se muestre en la vista
        $egreso = Egresos::find($egreso->id);

        //Se cambia el estado del egreso
        $egreso->Estado='0';
        $egreso->save();
        return redirect('/egresos');
    }
}
