<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cartera;
use Illuminate\Support\Facades\Redirect;
use DB;

class CarteraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Consulta que trae los ingresos y egresos
        $ingresosC = Cartera::where([
                        ['TipoMovimiento', '=', '1'],
                        ['Estado', '=', '1'],
                        ['EstadoMeta', '=', '0'],
                    ])->sum('valor');
                  
        
        $egresosC = Cartera::where([
                        ['TipoMovimiento', '=', '2'],
                        ['Estado', '=', '1'],
                    ])->sum('valor');


        //return $egresos & $ingresos;

        //return view('admin.cartera.index');
        return view('Admin.cartera.index', ['ingresosC' => $ingresosC], ['egresosC' => $egresosC] );
        

        
    }

    
}
