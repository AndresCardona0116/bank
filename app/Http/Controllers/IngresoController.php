<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingreso;
use Illuminate\Support\Facades\Redirect;
use DB;

class IngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $ingresos = Ingreso::join('users', 'movimientos.Usuario','=','users.id')
                    ->select('movimientos.id', 'movimientos.Nombre', 'movimientos.Valor', 'movimientos.Estado', 'movimientos.EstadoMeta', 'movimientos.TipoMovimiento', 'users.name')
                    ->where([
                        ['TipoMovimiento', '=', '1'],
                        ['Estado', '=', '1'],
                    ])->orderBy('Id', 'desc')
                    ->Get();

        return view('Admin.ingresos.index', ['ingresos' => $ingresos]);
        //return $ingresos;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Este metodo nos permite listar las categorias
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Se inserta nuevo ingrsos a la BD
        $ingresos = New Ingreso();
        $ingresos->Nombre = $request->Nombre;
        $ingresos->Valor = $request->Valor;
        $ingresos->Estado = '1';
        $ingresos->EstadoMeta = '0';
        $ingresos->TipoMovimiento = '1';
        $ingresos->Usuario = '1';
        $ingresos->save();
        return Redirect::to('ingresos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Editar los registros de los ingresos segun id 
        $ingresos = Ingreso::findOrFail($request->id_Ingreso);
        $ingresos->Nombre = $request->Nombre;
        $ingresos->Valor = $request->Valor;
        $ingresos->Estado = '1';
        $ingresos->EstadoMeta = '0';
        $ingresos->TipoMovimiento = '1';
        $ingresos->Usuario = '1';
        $ingresos->save();
        return Redirect::to('ingresos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ingreso $ingreso)
    {
        //Cambiar de estado algun ingreso 
        $ingreso = Ingreso::find($ingreso->id);

        //Se cambia el estado al post
        $ingreso->Estado='0';
        $ingreso->save();
        return redirect('/ingresos');
    }
}
