<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Egresos extends Model
{
    //Valores de los egresos 
    protected $table = 'movimientos';

    protected $fillable = ['Nombre', 'Valor', 'Estado', 'EstadoMeta', 'TipoMovimiento', 'Usuario'];
}
