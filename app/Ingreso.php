<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    //Modelo de los ingresos del sistema
    protected $table = 'movimientos';

    protected $fillable = ['Nombre', 'Valor', 'Estado', 'EstadoMeta', 'TipoMovimiento', 'Usuario'];
}
