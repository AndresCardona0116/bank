<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipomovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipomovimientos', function (Blueprint $table) {
            $table->id();
            $table->String('Tipo', 100);
            $table->timestamps();
        });

        DB::table('tipomovimientos')->insert(array('id'=>'1', 'Tipo'=>'Ingreso'));
        DB::table('tipomovimientos')->insert(array('id'=>'2', 'Tipo'=>'Egreso'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipomovimientos');
    }
}
