<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetasfinancierasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metasfinancieras', function (Blueprint $table) {
           
            $table->int('Id');
            $table->varchar('Nombre');
            $table->int ('Valor');
            $table->date ('FechaLiminte');
            $table->date ('FegraIngreso');
            $table->varchar  ('Descripcion');
            $table->int  ('Usuario');
            $table->boolean  ('Estado');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metasfinancieras');
    }
}
