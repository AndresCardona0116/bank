<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->id();
            $table->String('Nombre', 100);
            $table->Integer('Valor');
            $table->boolean('Estado')->default('1');
            $table->boolean('EstadoMeta')->default('1');
            $table->bigInteger('TipoMovimiento')->unsigned();
            $table->foreign('TipoMovimiento')
                ->references('id')->on('tipomovimientos');
            $table->bigInteger('Usuario')->unsigned();
            $table->foreign('Usuario')
                ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
